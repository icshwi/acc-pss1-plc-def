require_feature("VALIDITY-PV")

############################
#  STATUS BLOCK
############################

set_defaults(ARCHIVE=True, VALIDITY_PV="FEB-090:PSS-PLC-1:GCPU_ConnStat")
set_defaults(add_minor_alarm, add_major_alarm, ALARM_IF=False)
set_defaults(add_major_alarm, ALARM_IS_ANNUNCIATING=True, ALARM_IS_LATCHING=True)

define_status_block()

define_template("PASDoor", PV_ZRST="Open", PV_ONST="Closed", PV_TWST="Closed and Locked")

add_enum("PASDoor1", "INT",                           PV_NAME="Door1Stat", PV_DESC="PAS Door 1", TEMPLATE="PASDoor")
add_enum("PASDoor2", "INT",                           PV_NAME="Door2Stat", PV_DESC="PAS Door 2", TEMPLATE="PASDoor")
add_digital("FEBKeySlot",                             PV_NAME="FEBKeySlot", PV_DESC="A key is in the FEB slot of the key exchange unit 2")
add_digital("EntryKey1Locked",                        PV_NAME="EntryKey1Locked", PV_DESC="Entry key 1 is locked in the key exchange unit 2 slot 2")
add_digital("EntryKey2Locked",                        PV_NAME="EntryKey2Locked", PV_DESC="Entry key 2 is locked in the key exchange unit 2 slot 3")
add_digital("EntryKey3Locked",                        PV_NAME="EntryKey3Locked", PV_DESC="Entry key 3 is locked in the key exchange unit 2 slot 4")
add_digital("EntryKey4Locked",                        PV_NAME="EntryKey4Locked", PV_DESC="Entry key 4 is locked in the key exchange unit 2 slot 5")
add_digital("EntryKey1",                              PV_NAME="EntryKey1", PV_DESC="Entry key 1 in position On in slot 2 of key exchange unit 2")
add_digital("EntryKey2",                              PV_NAME="EntryKey2", PV_DESC="Entry key 2 in position On in slot 3 of key exchange unit 2")
add_digital("EntryKey3",                              PV_NAME="EntryKey3", PV_DESC="Entry key 3 in position On in slot 4 of key exchange unit 2")
add_digital("EntryKey4",                              PV_NAME="EntryKey4", PV_DESC="Entry key 4 in position On in slot 5 of key exchange unit 2")
add_digital("EntryInProgress",                        PV_NAME="EntryInProgress", PV_DESC="Entry to PSS1 controlled area through PAS is in progress")
add_digital("ExitInProgress",                         PV_NAME="ExitInProgress", PV_DESC="Exit from PSS1 controlled area thtough PAS is in progress.")
add_digital("EntryExitError",                         PV_NAME="EntryExitError", PV_DESC="Error detected upon entry or exit process - it shall be repeated.")
add_digital("CardReaderD1Off",                        PV_NAME="CardReaderD1Off", PV_DESC="Card reader on PAS door 1 disabled")
add_digital("CardReaderD2Off",                        PV_NAME="CardReaderD2Off", PV_DESC="Card reader on PAS door 2 disabled")
add_digital("CardReaderD1Unauthorised",               PV_NAME="CardReaderD1Unauthorised", PV_DESC="Person not authorised to enter in current mode of operation")
add_digital("CardReaderD1_SwipeOK",                   PV_NAME="CardReaderD1_SwipeOK", PV_DESC="Authorised person successfully swiped card reader 1")
add_digital("CardReaderD2_SwipeOK",                   PV_NAME="CardReaderD2_SwipeOK", PV_DESC="Authorised person successfully swiped card reader 2")
add_digital("PASD1_HMIblock",                         PV_NAME="D1_HMIblock", PV_DESC="PAS door 1 blocked from the PSS1 HMI")
add_digital("PASD2_HMIblock",                         PV_NAME="D2_HMIblock", PV_DESC="PAS door 2 blocked from the PSS1 HMI")
add_minor_alarm("PAS_PwrSuppOK", "PS failure",        PV_NAME="PwrSuppOK", PV_DESC="PAS - No power supply failure")
add_digital("PAS_BattLvlOK",                          PV_NAME="BattLvlOK", PV_DESC="PAS - Battery level OK (not low)")
add_minor_alarm("PAS_MainsupplyOK", "Mains failure",  PV_NAME="MainsupplyOK", PV_DESC="PAS - Main supply OK (no failure)")
add_digital("PAS_PresenceTimeOut",                    PV_NAME="PresenceTimeOut", PV_DESC="PAS - Maximum time inside the PAS exceeded")
add_digital("PAS_SolotekD1A",                         PV_NAME="SolotekD1A", PV_DESC="PAS - Single person entry ERROR (detected by Solotek on D1)")
add_digital("PAS_SolotekD2A",                         PV_NAME="SolotekD2A", PV_DESC="PAS - Single person exit ERROR (detected by Solotek on D2)")
add_digital("PAS_FloorMatZ1",                         PV_NAME="FloorMatZ1", PV_DESC="PAS - Single person check - object/person detected on pressure mat zone 1")
add_digital("PAS_FloorMatZ23",                        PV_NAME="FloorMatZ23", PV_DESC="PAS - Single person check - object/person detected on pressure mat zones 2 and 3")
add_digital("PAS_ObjectTIM100",                       PV_NAME="ObjectTIM100", PV_DESC="PAS - Object detected by TIM 100 (PAS not empty)")
add_digital("PAS_RadarIR",                            PV_NAME="RadarIR", PV_DESC="PAS - Object/person detected by IR radar (PAS not empty)")
add_digital("PAS_EntryCycleD1A",                      PV_NAME="EntryCycleD1A", PV_DESC="PAS - Entry cycle ongoing and OK (single person check OK, D1 and D2 closed)")
add_digital("PAS_ExitCycleD1A",                       PV_NAME="ExitCycleD1A", PV_DESC="PAS - Exit cycle OK and PAS empty (person successfyully left the controlled area through the PAS)")
add_digital("PAS_EntryCycleD2A",                      PV_NAME="EntryCycleD2A", PV_DESC="PAS - Entry cycle OK and PAS empty (person successfyully entered the controlled area throught the PAS)")
add_digital("PAS_ExitCycleD2A",                       PV_NAME="ExitCycleD2A", PV_DESC="PAS - Exit cycle ongoing and OK (single person check OK, D1  and D2 closed)")
add_digital("PAS_D3ALocked",                          PV_NAME="D3ALocked", PV_DESC="PAS - Mini-MAS door 1 locked")
add_digital("PAS_D3BLocked",                          PV_NAME="D3BLocked", PV_DESC="PAS - Mini-MAS door 2 locked")
add_digital("PAS_D3AClosed",                          PV_NAME="D3AClosed", PV_DESC="PAS - Mini-MAS door 1 Closed")
add_digital("PAS_D3BClosed",                          PV_NAME="D3BClosed", PV_DESC="PAS - Mini-MAS door 2 Closed")
add_digital("PAS_EmUnlockD1A",                        PV_NAME="EmUnlockD1A", PV_DESC="PAS - door 1 unlocked with overraide key")
add_major_alarm("PAS_EmUnlockD2A", "Unlocked w/ override key",  PV_NAME="EmUnlockD2A", PV_DESC="PAS - door 2 unlocked with overraide key", ALARM_IF=True)
add_digital("PAS_TooLongOpenD1A",                     PV_NAME="TooLongOpenD1A", PV_DESC="PAS - Door 1 detected open too long")
add_digital("PAS_TooLongOpenD2A",                     PV_NAME="TooLongOpenD2A", PV_DESC="PAS - Door 2 detected open too long")
add_digital("PAS_TooLongOpenD3A",                     PV_NAME="TooLongOpenD3A", PV_DESC="PAS - Mini-MAS door 1 open too long")
add_digital("PAS_TooLongOpenD3B",                     PV_NAME="TooLongOpenD3B", PV_DESC="PAS - Mini-MAS door 2 open too long")
add_digital("PAS_BreakGlassD1A",                      PV_NAME="BreakGlassD1A", PV_DESC="PAS - Break glass unit triggered - door 1 unlocked")
add_digital("PAS_LockSwitchD1A",                      PV_NAME="LockSwitchD1A", PV_DESC="PAS - Status of the lock on door 1")
add_digital("PAS_LockSwitchD2A",                      PV_NAME="LockSwitchD2A", PV_DESC="PAS - Status of the lock on door 2")
add_digital("PAS_LockSwitchD3A",                      PV_NAME="LockSwitchD3A", PV_DESC="PAS - Status of the lock on mini-MAS door 1")
add_digital("PAS_LockSwitchD3B",                      PV_NAME="LockSwitchD3B", PV_DESC="PAS - Status of the lock on mini-MAS door 2")
add_digital("PAS_BoxTamper",                          PV_NAME="BoxTamper", PV_DESC="PAS - Control box door detected open")
add_enum("PAS_EntryStep", "INT",                      PV_NAME="EntryStep", PV_DESC="Active step in the PAS entry cycle",  PV_ZRST="Idle",  PV_ONST="Card swipe (auth) OK",  PV_TWST="Door 1 open",  PV_THST="Door 1 closed and locked",  PV_FRST="Single person check OK",  PV_FVST="EPD in and activated",  PV_SXST="EPD out and OK",  PV_SVST="Entry key taken",  PV_EIST="Door 2 open",  PV_NIST="Door 2 closed and locked",  PV_TEST="PAS empty")
add_enum("PAS_ExitStep", "INT",                       PV_NAME="ExitStep", PV_DESC="Active step in the PAS entry cycle",  PV_ZRST="Idle",  PV_ONST="Card swipe OK",  PV_TWST="Door 2 open",  PV_THST="Door 2 closed and locked",  PV_FRST="Single person check OK",  PV_FVST="Entry key returned",  PV_SXST="Door 1 open",  PV_SVST="Door 1 closed and locked",  PV_EIST="PAS empty")
add_digital("PAS_Entry_KeyRequired",                  PV_NAME="Entry_Keyrequired", PV_DESC="Entry key required to enter the PSS1 controlled area")
add_digital("PAS_Exit_KeyRequired",                   PV_NAME="Exit_Keyrequired", PV_DESC="Entry key required to be returned to leave the PSS1 controlled area")
add_digital("PAS_InUse",                              PV_NAME="InUse", PV_DESC="PAS in use for entry or exit process")
add_digital("PAS_EKeyErrorDuringExit",                PV_NAME="EKeyErrorDuringExit",      PV_DESC="Entry Key inserted during a failed PAS exit cycle")
add_major_alarm("PAS_MajorEKeyFaultEntry",   "Entry without key",         PV_NAME="MajorEKeyFaultEntry",   PV_DESC="Entry key inserted  after being removed during a PAS entry cycle", ALARM_IF=True)
add_major_alarm("PAS_MajorEKeyFaultExit",    "Extra entry key inserted",  PV_NAME="MajorEKeyFaultExit",    PV_DESC="An additional entry key was inserted during a PAS exit cycle", ALARM_IF=True)
add_major_alarm("PAS_MajorBreakGlassAlarm",  "Break Glass unit used",     PV_NAME="MajorBreakGlassAlarm",  PV_DESC="PAS Break Glass unit used in a critical situation", ALARM_IF=True)
add_major_alarm("SIF5_PasD2LockError",       "PAS door lock failure",     PV_NAME="Door2LockError",        PV_DESC="SIF5 Pas Door 2 lockswitch error detected", ALARM_IF=True)


add_verbatim("""
record(calcout,  "[PLCF#INSTALLATION_SLOT]:#MiniMAS1Calc")
{
    field(SCAN, "1 second")
    field(INPA, "[PLCF#INSTALLATION_SLOT]:D3ALocked CP MSS")
    field(INPB, "[PLCF#INSTALLATION_SLOT]:D3AClosed CP MSS")
    field(CALC, "(A && B) ? 2 : B")
    field(OUT,  "[PLCF#INSTALLATION_SLOT]:MiniMAS1Stat PP MSS")
}


record(mbbi, "[PLCF#INSTALLATION_SLOT]:MiniMAS1Stat")
{
    field(ZRVL, "0")
    field(ZRST, "Open")

    field(ONVL, "1")
    field(ONST, "Closed")

    field(TWVL, "2")
    field(TWST, "Closed and Locked")
}


record(calcout,  "[PLCF#INSTALLATION_SLOT]:#MiniMAS2Calc")
{
    field(SCAN, "1 second")
    field(INPA, "[PLCF#INSTALLATION_SLOT]:D3BLocked CP MSS")
    field(INPB, "[PLCF#INSTALLATION_SLOT]:D3BClosed CP MSS")
    field(CALC, "(A && B) ? 2 : B")
    field(OUT,  "[PLCF#INSTALLATION_SLOT]:MiniMAS2Stat PP MSS")
}


record(mbbi, "[PLCF#INSTALLATION_SLOT]:MiniMAS2Stat")
{
    field(ZRVL, "0")
    field(ZRST, "Open")

    field(ONVL, "1")
    field(ONST, "Closed")

    field(TWVL, "2")
    field(TWST, "Closed and Locked")
}
""")

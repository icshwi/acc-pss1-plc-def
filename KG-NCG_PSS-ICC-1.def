require_feature("VALIDITY-PV")

############################
#  STATUS BLOCK
############################

set_defaults(ARCHIVE=True, VALIDITY_PV="FEB-090:PSS-PLC-1:GCPU_ConnStat")
set_defaults(add_minor_alarm, add_major_alarm, ALARM_IF=False)
set_defaults(add_major_alarm, ALARM_IS_ANNUNCIATING=True, ALARM_IS_LATCHING=True)

define_status_block()

add_major_alarm("RFQDTL1_fbkErrorC1", "Feedback error",   PV_NAME="RFQDTL1_fbkErrorC1",          PV_DESC="Feedback error from the RFQ-DTL1 contactor 1 (FALSE when error detected)")
add_major_alarm("RFQDTL1_fbkErrorC2", "Feedback error",   PV_NAME="RFQDTL1_fbkErrorC2",          PV_DESC="Feedback error from the RFQ-DTL1 contactor 2 (FALSE when error detected)")
add_major_alarm("RFQDTL1_fbkErrorUVR", "Feedback error",  PV_NAME="RFQDTL1_fbkErrorUVR",         PV_DESC="Feedback error from the RFQ-DTL1 modulator circuit breaker UVR (FALSE when error detected)")
add_digital("RFQDTL1_ActuatorsEnergised",                 PV_NAME="RFQDTL1_ActuatorsEnergised",  PV_DESC="RFQ-DTL1 modulator actuators are energised",  PV_ONAM="Energised")
add_digital("RFQDTL1_PermitOn",                           PV_NAME="RFQDTL1_PermitOn",            PV_DESC="RFQ-DTL1 permit to energize the actuators is active")
add_digital("RFQDTL1_fbkErrorCanReset",                   PV_NAME="RFQDTL1_fbkErrorCanReset",    PV_DESC="RFQ-DTL1 RF cell feedback error can be reset")


add_verbatim("""
record(calcout, "[PLCF#INSTALLATION_SLOT]:#CalcFbkError")
{
    field(DESC, "Aggregate feedback errors")
    field(INPA, "[PLCF#INSTALLATION_SLOT]:RFQDTL1_fbkErrorC1 CP MSS")
    field(INPB, "[PLCF#INSTALLATION_SLOT]:RFQDTL1_fbkErrorC2 CP MSS")
    field(INPC, "[PLCF#INSTALLATION_SLOT]:RFQDTL1_fbkErrorUVR CP MSS")
    field(CALC, "A && B && C")
    field(OUT,  "[PLCF#INSTALLATION_SLOT]:FbkError PP MSS")
    field(SCAN, "1 second")
}


record(bi, "[PLCF#INSTALLATION_SLOT]:FbkError")
{
    field(DESC, "If there is a feedback error")
    field(ZNAM, "Feedback error")
    field(ONAM, "Good")
}
""")

import os
import sys

wc = os.path.abspath(os.path.dirname(__file__))
sys.path.append(os.path.curdir)
sys.path.append(wc)


import helpers
from ccdb_factory import CCDB_Factory

accpss1a_git = "https://gitlab.esss.lu.se/icshwi/acc-pss1-plc-def"
accpss1a_wc  = None #wc
accpss1a_tag = None

factory = CCDB_Factory(accpss1a_tag)

ess_names = [
    "AccPSS:SC-IOC-001",
    "FEB-090:PSS-PLC-1",
    "FEB-090:PSS-GPLC-1",
    "FEB-010Row:CnPw-U-001",
    "FEB-010Row:CnPw-U-002",
    "FEB-010Row:CnPw-U-003",
    "FEB-010Row:CnPw-U-004",
    "AccPSS-Z0:PSS-RIO-4",
    "FEB-090:PSS-RIO-5",
    "FEB-010Row:CnPw-U-007",
    "AccPSS-Z0:PSS-GRly-1",
    "KG-NCG:PSS-ICC-1",
    "KG-NCG:PSS-LLRRB-1",
    "KG-NCG:PSS-LLRRB-5",
    "KG-NCG:PSS-ICC-2",
    "KG-NCG:PSS-LLRRB-6",
    "KG-NCG:PSS-LLRRB-7",
    "KG-NCG:PSS-ICC-3",
    "KG-NCG:PSS-LLRRB-8",
    "KG-NCG:PSS-ICC-4",
    "KG-NCG:PSS-LLRRB-2",
    "KG-NCG:PSS-LLRRB-3",
    "KG-NCG:PSS-LLRRB-4",
    "AccPSS:PSS-Area-1",
    "AccPSS-Z1:PSS-ESOS-1",
    "AccPSS-Z1:PSS-ESOS-2",
    "AccPSS-Z1:PSS-ESOS-3",
    "AccPSS-Z1:PSS-ESOS-4",
    "AccPSS-Z2:PSS-ESOS-1",
    "AccPSS-Z2:PSS-ESOS-2",
    "AccPSS-Z2:PSS-ESOS-3",
    "FEB-090:PSS-PAS-1",
    "FEB-090:PSS-MgS-2",
    "FEB-090:PSS-MeS-2",
    "FEB-090:PSS-MgS-1",
    "FEB-090:PSS-MeS-1",
    "FEB-090:PSS-MAS-1",
    "FEB-090:PSS-MgS-3",
    "FEB-090:PSS-MeS-3",
    "FEB-090:PSS-MgS-4",
    "FEB-090:PSS-MeS-4",
    "FEB-090:PSS-MgS-5",
    "FEB-090:PSS-MeS-5",
    "FEB-090:PSS-MgS-6",
    "FEB-090:PSS-MeS-6",
    "AccPSS-Z1:PSS-EscD-1",
    "AccPSS-Z1:PSS-MgS-1",
    "AccPSS-Z1:PSS-MeS-1",
    "AccPSS-Z1:PSS-MeL-1",
    "AccPSS-Z2:PSS-EscD-1",
    "AccPSS-Z2:PSS-MgS-3",
    "AccPSS-Z2:PSS-MeS-1",
    "AccPSS-Z2:PSS-MeL-1",
    "FEB-020Row:CnPw-U-005",
    "REMS-FEBRS:RMT-AMM-001",
    "REMS-FEBRS:RMT-AMM-002",
    "REMS-SPKRS:RMT-AMM-001",
    "REMS-S100RS:RMT-AMM-001",
    "REMS-S110RS:RMT-AMM-001",
    "KG-NCG:PSS-Area-1",
    "KG-NCG:PSS-WgS-1",
    "KG-NCG:PSS-WgS-2",
    "KG-NCG:PSS-WgS-3",
    "KG-NCG:PSS-WgS-4",
    "KG-NCG:PSS-WgS-5",
    "KG-NCG:PSS-WgS-6",
    "KG-NCG:PSS-WgS-7",
    "KG-NCG:PSS-WgS-8",
    "KG-NCG:PSS-WgS-9",
    "KG-NCG:PSS-WgS-10",
    "AccPSS-Z1:PSS-EscD-2",
    "AccPSS-Z2:PSS-MgS-1",
    "AccPSS-Z2:PSS-MgS-2",
    "AccPSS-Z1:PSS-Area-1",
    "AccPSS-Z2:PSS-Area-1",
    "FEB-020Row:CnPw-U-004",
    "MEBT-010:RFS-SS-110",
    "MEBT-010:RFS-SS-210",
    "MEBT-010:RFS-SS-310", ]


def addDevice(root, typ, name):
    try:
        ess_names.remove(name)
    except ValueError:
        raise RuntimeError("{} is not amongst the ESS names".format(name))
    filename = helpers.sanitizeFilename(name)
    device = root.addDevice(typ, name)
    device.addLink("EPI[{}]".format(filename), accpss1a_git, accpss1a_wc)
#    device.addLink("BEAST TEMPLATE[{}]".format(filename), accpss1a_git, accpss1a_wc)
    return device


# IOC
name = "AccPSS:SC-IOC-001"
ioc = factory.addDevice("IOC", name)
ess_names.remove(name)
ioc.setProperty("EPICSVersion", "7.0.5")
ioc.setProperty("E3RequireVersion", "3.4.1")
ioc.addLink("IOC_REPOSITORY", "https://gitlab.esss.lu.se/iocs/pss/accpss_sc_ioc_001.git", download = False)


#
# Gateway PLC
#
name = "FEB-090:PSS-GPLC-1"
plc = ioc.addPLC(name)
ess_names.remove(name)
name = helpers.sanitizeFilename(name)
plc.addLink("EPI[{}]".format(name), accpss1a_git, accpss1a_wc)
#plc.addLink("BEAST TEMPLATE[{}]".format(name), accpss1a_git, accpss1a_wc)
plc.setProperty("PLCF#PLC-EPICS-COMMS: InterfaceID", "72")
#plc.setProperty("PLC-EPICS-COMMS: PLCPulse", "Pulse_100ms")
plc.setProperty("PLCF#PLC-DIAG:Max-IO-Devices", "10")
plc.setProperty("PLCF#PLC-DIAG:Max-Local-Modules", "30")
plc.setProperty("PLCF#PLC-DIAG:Max-Modules-In-IO-Device", "30")
plc.setProperty("PLCF#PLC-EPICS-COMMS: GatewayDatablock", "dbStandardtoGateway.data")
plc.setProperty("Hostname", "pss1-plc-gcpu.tn.esss.lu.se")


#
# Safety PLC
#
splc = addDevice(plc, "PSS1_PROCESS_PLC", "FEB-090:PSS-PLC-1")


#
# Areas
#
addDevice(splc, "KG_NCG_PSS_AREA_1", "KG-NCG:PSS-Area-1")
addDevice(splc, "AccPSS_PSS_AREA_1", "AccPSS:PSS-Area-1")
addDevice(splc, "AccPSS_Z1_PSS_Area_1", "AccPSS-Z1:PSS-Area-1")
addDevice(splc, "AccPSS_Z2_PSS_Area_1", "AccPSS-Z2:PSS-Area-1")


#
# Grounding relay
#
addDevice(splc, "GRLY", "AccPSS-Z0:PSS-GRly-1")


#
# Cabinets
#
for cabinet_name in [ "FEB-010Row:CnPw-U-001", "FEB-010Row:CnPw-U-002", "FEB-010Row:CnPw-U-003", "FEB-010Row:CnPw-U-004", "FEB-010Row:CnPw-U-007", "FEB-020Row:CnPw-U-004", "FEB-020Row:CnPw-U-005" ]:
    addDevice(splc, "CABINET", cabinet_name)


#
# Magnetic switches
#
#for switch_name in [ "AccPSS-Z1:PSS-MgS-1", "AccPSS-Z2:PSS-MgS-1", "AccPSS-Z2:PSS-MgS-3", "FEB-090:PSS-MgS-1", "FEB-090:PSS-MgS-2", "FEB-090:PSS-MgS-3", "FEB-090:PSS-MgS-4", "FEB-090:PSS-MgS-5", "FEB-090:PSS-MgS-6" ]:
for switch_name in [ "AccPSS-Z1:PSS-MgS-1", "AccPSS-Z2:PSS-MgS-1", "AccPSS-Z2:PSS-MgS-2", "AccPSS-Z2:PSS-MgS-3", "FEB-090:PSS-MgS-1", "FEB-090:PSS-MgS-2", "FEB-090:PSS-MgS-3", "FEB-090:PSS-MgS-4", "FEB-090:PSS-MgS-5", "FEB-090:PSS-MgS-6" ]:
    addDevice(splc, "MAGNETIC_SWITCH", switch_name)


#
# Mechanical switches
#
for switch_name in [ "AccPSS-Z1:PSS-MeS-1", "AccPSS-Z2:PSS-MeS-1", "FEB-090:PSS-MeS-1", "FEB-090:PSS-MeS-2", "FEB-090:PSS-MeS-3", "FEB-090:PSS-MeS-4", "FEB-090:PSS-MeS-5", "FEB-090:PSS-MeS-6" ]:
    addDevice(splc, "MECHANICAL_SWITCH", switch_name)


#
# Waveguide switches
#
for switch_name in [ "KG-NCG:PSS-WgS-1", "KG-NCG:PSS-WgS-2", "KG-NCG:PSS-WgS-3", "KG-NCG:PSS-WgS-4", "KG-NCG:PSS-WgS-5", "KG-NCG:PSS-WgS-6", "KG-NCG:PSS-WgS-7", "KG-NCG:PSS-WgS-8", "KG-NCG:PSS-WgS-9", "KG-NCG:PSS-WgS-10" ]:
    addDevice(splc, "WAVEGUIDE_SWITCH", switch_name)


#
# Mechanical latches
#
for latch_name in [ "AccPSS-Z1:PSS-MeL-1", "AccPSS-Z2:PSS-MeL-1" ]:
    addDevice(splc, "MECHANICAL_LATCH", latch_name)


#
# Access stations
#
for station_name in [ "FEB-090:PSS-MAS-1", "FEB-090:PSS-PAS-1" ]:
    addDevice(splc, station_name, station_name)


#
# LLRRBs
#
for llrrb_name in [ "KG-NCG:PSS-LLRRB-1", "KG-NCG:PSS-LLRRB-2", "KG-NCG:PSS-LLRRB-3", "KG-NCG:PSS-LLRRB-4", "KG-NCG:PSS-LLRRB-5", "KG-NCG:PSS-LLRRB-6", "KG-NCG:PSS-LLRRB-7", "KG-NCG:PSS-LLRRB-8" ]:
    addDevice(splc, "LLRRB", llrrb_name)


#
# ESOSs
#
for esos_name in [ "AccPSS-Z1:PSS-ESOS-1", "AccPSS-Z1:PSS-ESOS-2", "AccPSS-Z1:PSS-ESOS-3", "AccPSS-Z1:PSS-ESOS-4", "AccPSS-Z2:PSS-ESOS-1", "AccPSS-Z2:PSS-ESOS-2", "AccPSS-Z2:PSS-ESOS-3" ]:
    addDevice(splc, "ESOS", esos_name)


#
# ICCs
#
for icc_name in [ "KG-NCG:PSS-ICC-1", "KG-NCG:PSS-ICC-2", "KG-NCG:PSS-ICC-3", "KG-NCG:PSS-ICC-4" ]:
    addDevice(splc, "ICC", icc_name)


#
# Escape doors
#
for escd_name in [ "AccPSS-Z1:PSS-EscD-1", "AccPSS-Z2:PSS-EscD-1", "AccPSS-Z1:PSS-EscD-2" ]:
    addDevice(splc, "ESCAPE_DOOR", escd_name)


#
# RIO devices
#
for rio_name in [ "AccPSS-Z0:PSS-RIO-4", "FEB-090:PSS-RIO-5" ]:
    addDevice(splc, "RIO", rio_name)


#
# Radiation monitors
#
for rad_name in [ "REMS-FEBRS:RMT-AMM-001", "REMS-FEBRS:RMT-AMM-002", "REMS-S100RS:RMT-AMM-001", "REMS-S110RS:RMT-AMM-001", "REMS-SPKRS:RMT-AMM-001" ]:
    addDevice(splc, "RAD", rad_name)

#
# MEBT shutter switches
#
for (mebt_id, shutter_switch_name) in enumerate([ "MEBT-010:RFS-SS-110", "MEBT-010:RFS-SS-210", "MEBT-010:RFS-SS-310" ]):
    ess_names.remove(shutter_switch_name)
    shutter_switch = splc.addDevice("MEBT_SHUTTER_SWITCHES", shutter_switch_name)
    shutter_switch.setProperty("MEBT_ID", mebt_id + 1)
    shutter_switch.addLink("EPI", accpss1a_git, accpss1a_wc)

if ess_names:
    print("Unused ESS names:", ess_names)

factory.dump("acc_pss1")
